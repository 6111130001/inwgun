package com.example.guntum;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class Home extends AppCompatActivity {
    String url = "http://business-bulletin-service.co.th:8080/thena/api/v1/_thena_user";
    ArrayList<String> id = new ArrayList<>();
    ArrayList<String> whoAreYou = new ArrayList<>();
    private ListView listview;
    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        bundle = getIntent().getExtras();
        bundle.getString("Token");//รับเข้า


        Button btt1 = findViewById(R.id.button2);
        listview = (ListView) findViewById(R.id.listview);

        whoAreYou = getIntent().getStringArrayListExtra("sentUserName");
        id = getIntent().getStringArrayListExtra("sentID");
        Log.d("HomeTag"," id ArrryList = "+ id);
        Log.d("HomeTag"," "+whoAreYou);


        btt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d("HomeTag","ได้รับtoken = "+ bundle.getString("Token"));//รับ -1-

                UnitedDataConnection unitedDataConnection = new UnitedDataConnection(url,"GET",bundle.getString("Token"));
//**--*-*-*-*-*-*-*-
                id = unitedDataConnection.id;
                whoAreYou= unitedDataConnection.whoAreYou;
                Log.d("HomeTag","whoAreYou = "+whoAreYou);
                listview.setAdapter(new EfficientAdapter(getApplicationContext()));

           }
        });

    }




    public class EfficientAdapter extends BaseAdapter{
        public Context mcontext;//ใช้ในการโหลดLayOut
        public LayoutInflater mInflater;//ใช้ในการโหลดLayOut

        public  EfficientAdapter(Context context){
            mcontext = context;
            mInflater = LayoutInflater.from(mcontext);


        }
        @Override
        public int getCount() {
            return whoAreYou.size();//จำนวนฟีด
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            if (convertView == null){
                //load layout
                convertView =mInflater.inflate(R.layout.layout,null);
                holder = new ViewHolder();
                holder.title = convertView.findViewById(R.id.textView2);
                convertView.setTag(holder);
            }else{
                //rebind widget
                holder = (ViewHolder) convertView.getTag();
            }
            holder.title.setText("ชื่อขนามสกุล = "+whoAreYou.get(position)+"\n"+" ID ="+id.get(position));
         //   holder.title.setText(" ID ="+id.get(position));

            return convertView;
        }
    }

    public class ViewHolder{
        TextView title;
    }
}