package com.example.guntum;

import android.content.Intent;
import android.net.UrlQuerySanitizer;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class UnitedDataConnection {
//UnitedData();
    JSONArray userNameGuest;
    ArrayList<String> id = new ArrayList<>();
    ArrayList<String> whoAreYou = new ArrayList<>();

    Intent mess;
    String fusion;
    String url;
    String postOrGet;
    String theToken;
    String userToken;
    TalkToServer talkToServer = new TalkToServer();


    public UnitedDataConnection(String url, String postOrGet, String theToken) {
    this.url = url;this.postOrGet = postOrGet;this.theToken = theToken;

Log.d("UnitedTag","the Token = "+this.theToken);
        try {
            if(this.theToken == null) {
                Log.d("UnitedTag","the Token = null");
             talkToServer.execute(url,this.theToken,postOrGet).get();

            }else talkToServer.execute(url,this.theToken,postOrGet).get();

        } catch (ExecutionException e) {
        e.printStackTrace();
        } catch (InterruptedException e) {
        e.printStackTrace();
    }
    }


    private class TalkToServer extends AsyncTask<String, Void, Void> {
        String asyUrl,asytheToken,asyPostOrGet;

        //String anUrl,anPostOrGet,gUrl;
        ConnectionPipe dataJson = new ConnectionPipe();
        int connectionRespond = 0;


String respond;
        @Override
        protected Void doInBackground(String... strings) {
            asyUrl = strings[0];
            asytheToken = strings[1];
            asyPostOrGet = strings[2];
            JSONObject json = new JSONObject();


            try {

                 if(asytheToken == null){
                         URL obj = new URL(asyUrl+"/authenticate");
                         HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                         con.setRequestMethod(asyPostOrGet);//poss
                         con.setDoOutput(true);

                    try{
                        json.put("userId","guest");
                        json.put("password","guest");
                    }catch(JSONException e){
                        Log.e("UnitedTag", "Error   " +e.toString());
                    }

                         dataJson.dataOutPut(con, json);


                    connectionRespond = con.getResponseCode();

                    if (connectionRespond == HttpURLConnection.HTTP_OK){//สำเร็จ

                        // ทำการรับtoken
                      UnitedDataConnection.this.userToken = dataJson.inPutReturnToString(con,"token");//รับtokenและแปลงเป็นสติงแล้ว
                        Log.d("UnitedTag",""+ UnitedDataConnection.this.userToken);
                        //ได้tokenและส่งต่อได้แล้ว
                    }else Log.d("UnitedTag","connectionRespond = "+connectionRespond);

                 }

////////////////////////////////

                else if(asytheToken!=null){
                     URL obj = new URL(asyUrl);
                     HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                     con.setRequestMethod(asyPostOrGet);//poss
                     con.setRequestProperty("Authorization","Bearer "+asytheToken);

                    connectionRespond = con.getResponseCode();
                    if (connectionRespond == HttpURLConnection.HTTP_OK) {//สำเร็จ

                        userNameGuest = dataJson.inPutReturnToJSONArray(con,"data");
                       //ข้างล่างนี่คือแปลและรับค่าwhoAreYou
                        for(int i =0;i<userNameGuest.length();i++) {

                            JSONObject temp = (JSONObject) userNameGuest.get(i);


                            id.add((String) temp.get("userId"));
                            fusion = temp.get("firstname")+" "+temp.get("lastname");
                           UnitedDataConnection.this.whoAreYou.add(fusion);

                            Log.d ("UnitedTag","+ "+ whoAreYou.get(i)+" id = "+id.get(i));


                        }

                        Log.d ("UnitedTag","comeOn"+ UnitedDataConnection.this.whoAreYou);

                    }else Log.d("UnitedTag","connectionRespond = "+connectionRespond);

                 }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            super.onPostExecute(unused);

            Log.d("UnitedTag",""+this.connectionRespond);


        }
    }
}
